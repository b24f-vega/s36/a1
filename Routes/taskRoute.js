const express = require("express");
const router = express.Router();

const taskController = require("../Controllers/taskControllers.js")



/*
 Routes
*/

//Route for getAll
router.get("/get", taskController.getAll);

//Route for createTask
router.post("/addTask", taskController.createTask);


//Route for deleteTask
router.delete("/deleteTasks/:id", taskController.deleteTask);

//Route for updateTask
router.get("/findTask/:id", taskController.findTask);

//Route for updateTask
router.put("/updateTask/:id/completed", taskController.updateTask);







module.exports = router;
